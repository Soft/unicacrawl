from setuptools import setup, find_packages

import unicacrawl

with open("README.md", "r") as handle:
    long_description = handle.read()

setup(name="unicacrawl",
      author=unicacrawl.__author__,
      author_email=unicacrawl.__email__,
      version=unicacrawl.__version__,
      description=unicacrawl.__doc__,
      long_description=long_description,
      packages = find_packages(),
      entry_points={
          "console_scripts": [
              "unicacrawl = unicacrawl.command:main"
          ]
      },
      install_requires=[
          "Scrapy==1.4",
          "coloredlogs==7.1",
          "isoweek==1.3",
          "click==6.7",
          "SQLAlchemy==1.1"
      ],
      keywords=["crawler", "restaurant", "menu", "food"],
      license=unicacrawl.__license__)
