FROM python:3.6

WORKDIR /usr/src/app
COPY . .
RUN pip install . && \
    mkdir -p /var/lib/unicacrawl && \
    groupadd -r unicacrawl  && \
    useradd -r -g unicacrawl unicacrawl && \
    chown unicacrawl:unicacrawl /var/lib/unicacrawl
VOLUME /var/lib/unicacrawl

ENV SINK=file SINK_OPTIONS=/var/lib/unicacrawl/meals.json

USER unicacrawl:unicacrawl
CMD [ "sh", "-c", "unicacrawl \"$SINK\" \"$SINK_OPTIONS\"" ]
