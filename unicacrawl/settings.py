import logging
import unicacrawl

# Default settings
SETTINGS_TEMPLATE = {
    "BOT_NAME": "unicacrawl",
    "USER_AGENT":  "unicacrawl/{}".format(unicacrawl.__version__),
    "LOG_ENABLED": False # We setup logging on our own
}
