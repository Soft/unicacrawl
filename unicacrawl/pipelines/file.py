import json
from json import JSONEncoder
from enum import Enum
from datetime import date, datetime

OUTPUT_KEY = "FILE_PATH"

class Encoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        elif isinstance(obj, Enum):
            return obj.name
        elif isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, datetime):
            return obj.isoformat(" ")
        else:
            return super().default(obj)

class FilePipeline:
    def __init__(self, path):
        self.items = []
        self.path = path

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings[OUTPUT_KEY])

    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item

    def close_spider(self, spider):
        with open(self.path, "w") as handle:
            handle.write(json.dumps(self.items, cls=Encoder, indent=4))
