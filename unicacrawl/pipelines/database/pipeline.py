import logging
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import unicacrawl.items as items
from unicacrawl.pipelines.database.models import Base, Scrape, Meal, Translation, Price

CONNECTION_KEY = "CONNECTION_STRING"

class DatabasePipeline:
    def __init__(self, connection_string):
        self.connection_string = connection_string
        self.scrape = None
        self.engine = create_engine(self.connection_string)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(self.engine)
        self.session = Session()
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings[CONNECTION_KEY])

    def open_spider(self, spider):
        scrape = Scrape(start_time=datetime.now(), meals=[])
        self.scrape = scrape
        self.session.add(scrape)
        self.session.commit()

    def process_item(self, item, spider):
        meal = self.session.query(Meal) \
                           .filter_by(date=item["date"],
                                      restaurant=item["restaurant"],
                                      seq=item["sequence"]) \
                           .first()
        if meal is None:
            meal = self.__create_meal(item)
        self.__create_translation(meal, item)
        self.session.commit()
        return item

    def close_spider(self, spider):
        self.scrape.end_time = datetime.now()
        self.session.commit()

    def __create_meal(self, item):
        logging.info("Adding new meal for the %s at %s", item["date"], item["restaurant"])
        meal = Meal(date=item["date"], restaurant=item["restaurant"], seq=item["sequence"])
        self.__update_from_limitations(meal, item["limitations"])
        prices = [Price(seq=i, value=amount)
                  for i, amount in enumerate(item["prices"])]
        meal.prices = prices
        meal.translations = []
        self.scrape.meals.append(meal)
        self.session.add(meal)
        return meal
    
    def __create_translation(self, meal, item):
        translation = Translation(description=item["description"],
                                  language=item["language"])
        meal.translations.append(translation)

    def __update_from_limitations(self, meal, limitations):
        for limitation in limitations:
            if limitation == items.Limitation.lactose_free:
                meal.lactose_free = True
            elif limitation == items.Limitation.low_lactose:
                meal.low_lactose = True
            elif limitation == items.Limitation.milk_free:
                meal.milk_free = True
            elif limitation == items.Limitation.gluten_free:
                meal.gluten_free = True
            elif limitation == items.Limitation.allergens:
               meal.allergens = True

