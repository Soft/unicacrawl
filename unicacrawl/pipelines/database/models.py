from sqlalchemy import Column, ForeignKey, UniqueConstraint
from sqlalchemy.types import String, Integer, Boolean, Float, Enum, Date, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

import unicacrawl.items as items

Base = declarative_base()

class Scrape(Base):
    __tablename__ = "scrapes"
    id = Column(Integer, primary_key=True)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    meals = relationship("Meal", back_populates="scrape")

class Meal(Base):
    __tablename__ = "meals"
    __table_args__ = (UniqueConstraint("id", "seq", "date", "restaurant"),)
    id = Column(Integer, primary_key=True)
    scrape_id = Column(Integer, ForeignKey("scrapes.id"))
    scrape = relationship("Scrape", back_populates="meals")

    date = Column(Date)
    restaurant = Column(String)
    seq = Column(Integer, nullable=False)
    translations = relationship("Translation", back_populates="meal")
    prices = relationship("Price", back_populates="meal")

    lactose_free = Column(Boolean, default=False)
    low_lactose = Column(Boolean, default=False)
    milk_free = Column(Boolean, default=False)
    gluten_free = Column(Boolean, default=False)
    allergens = Column(Boolean, default=False)
    vegan = Column(Boolean, default=False)

class Translation(Base):
    __tablename__ = "translations"
    meal_id = Column(Integer, ForeignKey("meals.id"), primary_key=True)
    language = Column(Enum(items.Language), primary_key=True)
    description = Column(String)
    meal = relationship("Meal", back_populates="translations")

class Price(Base):
    __tablename__ = "prices"
    meal_id = Column(Integer, ForeignKey("meals.id"), primary_key=True)
    seq = Column(Integer, nullable=False, primary_key=True)
    value = Column(Float)
    meal = relationship("Meal", back_populates="prices")
