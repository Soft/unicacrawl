from enum import Enum, unique
from scrapy import Item, Field

@unique
class Language(Enum):
    finnish = 1
    english = 2
    swedish = 3

    @staticmethod
    def from_code(lang):
        {
            "fi": Language.finnish,
            "en": Language.english,
            "se": Language.swedish
        }.get(lang)

@unique
class Limitation(Enum):
    lactose_free = 1
    low_lactose = 2
    milk_free = 3
    gluten_free = 4
    allergens = 5
    vegan = 6

    @staticmethod
    def from_code(code):
        {
            "L": Limitation.lactose_free,
            "VL": Limitation.low_lactose,
            "M": Limitation.milk_free,
            "G": Limitation.gluten_free,
            "A": Limitation.allergens,
            "VEG": Limitation.vegan
        }.get(code)

class Meal(Item):
    date = Field()
    restaurant = Field()
    sequence = Field()
    description = Field()
    limitations = Field()
    prices = Field()
    language = Field()

