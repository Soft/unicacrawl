import logging
import coloredlogs
import click
import unicacrawl
from scrapy.crawler import CrawlerProcess
from unicacrawl.spider import UnicaSpider
from unicacrawl.settings import SETTINGS_TEMPLATE
from unicacrawl.pipelines.file import OUTPUT_KEY
from unicacrawl.pipelines.database import CONNECTION_KEY

# TODO: We could support using multiple sinks at once
def settings_from_args(sink, options):
    settings = SETTINGS_TEMPLATE.copy()
    if sink == "file":
        settings[OUTPUT_KEY] = options
        settings["ITEM_PIPELINES"] = \
        {"unicacrawl.pipelines.file.FilePipeline": 100}
    elif sink == "database":
        settings[CONNECTION_KEY] = options
        settings["ITEM_PIPELINES"] = \
        {"unicacrawl.pipelines.database.DatabasePipeline": 100}
    return settings

@click.command()
@click.argument("sink", type=click.Choice(("file", "database")))
@click.argument("sink-options")
@click.version_option(unicacrawl.__version__)
def main(**arguments):
    coloredlogs.install()
    logging.root.setLevel(logging.INFO)

    settings = settings_from_args(arguments["sink"], arguments["sink_options"])

    crawler = CrawlerProcess(settings)
    crawler.crawl(UnicaSpider)
    crawler.start()

