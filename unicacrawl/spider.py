import re
import logging
from datetime import datetime
from urllib.parse import urlparse
from scrapy import Spider, Request
from isoweek import Week

from unicacrawl.items import Meal, Limitation, Language

RESTAURANTS = {"assarin-ullakko", "delica", "dental",
               "macciavelli", "nutritio", "tottisalmi",
               "brygge", "deli-pharma", "galilei",
               "mikro", "ruokakello"}

LANG_URLS = {Language.finnish: "http://www.unica.fi/fi/ravintolat/",
             Language.english: "http://www.unica.fi/en/restaurants/",
             Language.swedish: "http://www.unica.fi/se/restauranger/"}


class UnicaSpider(Spider):
    """Spider for retrieving information about food served by Unica restaurants"""

    name = "unica"
    allowed_domains = ("unica.fi",)

    def start_requests(self):
        return (Request(base + tag, meta={"language": lang, "restaurant": tag})
                for tag in RESTAURANTS
                for lang, base in LANG_URLS.items())

    def parse(self, response):
        language = response.meta["language"]
        restaurant = response.meta["restaurant"]
        week_number = self.__week_number(response)
        days = response.css(".menu-list > .accord")
        for day in days:
            week_day = self.__week_day(day)
            date = self.__date(week_number, week_day)
            rows = day.css("tr")
            for seq, row in enumerate(rows):
                meal_info = self.__meal(row)
                if meal_info is not None:
                    description, limitations, prices = meal_info
                    yield Meal(date=date,
                               restaurant=restaurant,
                               sequence=seq,
                               description=description,
                               limitations=limitations,
                               prices=prices,
                               language=language)
    
    def __week_number(self, response):
        header = response.css(".pad > h3:first-child::text").extract_first()
        if header is not None:
            matches = re.search("(\d+)", header)
            return matches is not None and int(matches.group(1))
    
    def __limitations(self, column):
        # FIXME: Sometimes the limitations column includes things like "VEG / M"
        # How should we handle these?
        codes = column.css("span::text").extract()
        return {Limitation.from_code(code) for code in codes}
    
    def __prices(self, string):
        return tuple(float(p.group(1).replace(",", ".")) for p
                     in re.finditer("(\d+,\d+)", string))

    def __meal(self, row):
        lunch = row.css(".lunch::text").extract_first()
        limitations = row.css(".limitations")
        prices = row.css(".price::text").extract_first()
        if lunch is not None \
           and limitations is not None \
           and prices is not None: 
            prices = self.__prices(prices)
            limitations = self.__limitations(limitations)
            return (lunch, limitations, prices)

    def __week_day(self, day):
        day = day.css("h4::attr(data-dayofweek)").extract_first()
        return day is not None and day.isdigit() and int(day)

    def __language(self, url):
        lang = urlparse(url).path.split("/")[1]
        return Language.from_code(lang)
    
    def __date(self, week, day):
        year = datetime.now().year
        return Week(year, week).day(day)

