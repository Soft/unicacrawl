# unicacrawl 🍔

unicacrawl is a library and a tool for retrieving information about food served by Unica restaurants in the Turku area.

# Installation

The simples way to install unicacrawl is by using `pip`:

    pip install git+https://bitbucket.org/Soft/unicacrawl.git

Additionally, unicacrawl can be trivially packaged into a Docker image:

    git clone https://bitbucket.org/Soft/unicacrawl.git
    cd unicacrawl
    docker build -t unicacrawl .

# Usage

The included unicacrawl tool supports saving retrieved meal information into a file or a database. To download currently available meal information, run:

    unicacrawl file output.json

Alternativelly, to save the data into a database:

    unicacrawl database 'sqlite:///database.sqlite'

The second argument has to be [SQLAlchemy compatible connection string.](http://docs.sqlalchemy.org/en/latest/core/engines.html). This means that unicacrawl can be used with various database backends, including SQLite and PostgreSQL.

```
Usage: unicacrawl [OPTIONS] SINK SINK_OPTIONS

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.
```

# Database Schema

![model](resources/model.png)

# Known Issues

- Sometimes information about dietary restrictions contains more complex expressions. These can cause unicacrawl to collect confusing data.
- If the different translations contain meals in different order, it will cause the database backend to incorrectly think them as the same meal.